﻿using ImageSearch.WebApp.Extensions;
using ImageSearch.WebApp.Models;
using ImageSearch.WebApp.Models.Search;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ImageSearch.WebApp.Controllers
{
    [Authorize]
    public class ImageSearchController : Controller
    {
        private readonly ILogger<ImageSearchController> _logger;
        private IConfiguration _Configure;
        private string apiBaseUrl;
        HttpClient client;
        public ImageSearchController(ILogger<ImageSearchController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");

            client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUrl + "BingImageSearch")
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        }
        
        /// <summary>
        /// Load data based on the parameter values, returns list of imageinfo objects
        /// </summary>
        /// <param name="imageSearchParameters"></param>
        /// <returns></returns>
        public async Task<IActionResult> Index(ImageSearchParameters imageSearchParameters)
        {
            List<ImageInfo> ImageList = new List<ImageInfo>();
            StringBuilder locationQuery = new StringBuilder();

            //-- Build query string
            if (!string.IsNullOrEmpty(imageSearchParameters.LocationName))
            {
                locationQuery.Append(imageSearchParameters.LocationName);
            }
            else if (!string.IsNullOrEmpty(imageSearchParameters.Latitude.ToString()) &&
                !string.IsNullOrEmpty(imageSearchParameters.Longitude.ToString()))
            {
                locationQuery.Append(imageSearchParameters.Latitude + "," + imageSearchParameters.Longitude);
            }

            //-- Call APIs
            if (!string.IsNullOrEmpty(imageSearchParameters.Query))
            {
                if (imageSearchParameters.DataSource.ToLower() == "db")
                {
                    using (var response = await client.GetAsync(apiBaseUrl + "Image/" + imageSearchParameters.Query))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        ImageList = JsonConvert.DeserializeObject<List<ImageInfo>>(apiResponse);
                    }
                }
                else if (imageSearchParameters.DataSource.ToLower() == "api")
                {
                    using (var response = await client.GetAsync(apiBaseUrl + "BingImageSearch/query=" + imageSearchParameters.Query))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        ImageList = JsonConvert.DeserializeObject<List<ImageInfo>>(apiResponse);
                    }
                }         

                //-- Store results, fetched in the view
                TempData.Put("Images", ImageList);
            }
            
            return View("~/Views/ImageSearch/Index.cshtml", imageSearchParameters);
        }

        /// <summary>
        /// Displays details of the image, analyse visual Features.
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="imageSource"></param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("Detail")]
        public async Task<IActionResult> DetailGet(string imageUrl, string imageSource)
        {
            ComputerVisionClient computerVision = new ComputerVisionClient(
                new Microsoft.Azure.CognitiveServices.Vision.ComputerVision.ApiKeyServiceClientCredentials(_Configure["Keys:ComputerVision"]));
            List<VisualFeatureTypes> visualFeatures = new List<VisualFeatureTypes>()
            {
                VisualFeatureTypes.Adult,
                VisualFeatureTypes.Categories,
                VisualFeatureTypes.Color,
                VisualFeatureTypes.Description,
                VisualFeatureTypes.Faces,
                VisualFeatureTypes.ImageType,
                VisualFeatureTypes.Tags
            };
            computerVision.Endpoint = _Configure["Keys:Endpoint"];

            ImageAnalysis imageAnalysis = await computerVision.AnalyzeImageAsync(imageUrl, visualFeatures);

            //-- Store results, fetched in the view
            TempData["ImageUrl"] = imageUrl;
            TempData["ImageSource"] = imageSource;

            return View("~/Views/ImageSearch/Detail.cshtml", imageAnalysis);
        }
    }
}
