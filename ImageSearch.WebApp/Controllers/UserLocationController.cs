﻿using ImageSearch.WebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ImageSearch.WebApp.Controllers
{
    [Authorize]
    public class UserLocationController : Controller
    {
        private readonly ILogger<UserLocationController> _logger;
        private IConfiguration _Configure;
        private string apiBaseUrl;

        HttpClient client;
        public UserLocationController(ILogger<UserLocationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");

            client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUrl + "UserLocation")
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        }
     
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            List<UserLocation> UserLocationList = new List<UserLocation>();
            using (var response = await client.GetAsync(apiBaseUrl + "UserLocation/"+ User.Identity.Name))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();

                UserLocationList = JsonConvert.DeserializeObject<List<UserLocation>>(apiResponse);           
            }

            foreach(var userLoc in UserLocationList)
            {
                var item = UserLocationList.Where(x => x.LocationId == userLoc.LocationId);
                using (var response = await client.GetAsync(apiBaseUrl + "Location/id/" + userLoc.LocationId))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    Location userLocation = JsonConvert.DeserializeObject<Location>(apiResponse);
                    if(userLocation !=null && userLocation.LocationID > 0)
                    {
                        userLoc.Location = userLocation;
                    }
                }
            }

            return View(UserLocationList);
        }
        public ViewResult GetUserLocation() => View();

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetUserLocation(int id)
        {
            UserLocation userlocation = new UserLocation();

            using (var response = await client.GetAsync(apiBaseUrl + "UserLocation/" + id))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                userlocation = JsonConvert.DeserializeObject<UserLocation>(apiResponse);
            }

            return View(userlocation);
        }

        public ViewResult AddUserLocation() => View();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userlocation"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddUserLocation(UserLocation userlocation)
        {
            UserLocation receivedUserLocation = new UserLocation();

            StringContent content = new StringContent(JsonConvert.SerializeObject(userlocation), Encoding.UTF8, "application/json");

            using (var response = await client.PostAsync(apiBaseUrl + "UserLocation", content))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                receivedUserLocation = JsonConvert.DeserializeObject<UserLocation>(apiResponse);
            }

            return View(receivedUserLocation);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> UpdateUserLocation(int id)
        {
            UserLocation UserLocation = new UserLocation();

            using (var response = await client.GetAsync(apiBaseUrl + "UserLocation/" + id))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                UserLocation = JsonConvert.DeserializeObject<UserLocation>(apiResponse);

            }

            return View(UserLocation);
        }

        /// <summary>
        /// Removes user location
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DeleteUserLocation(int id)
        {
            using (var response = await client.DeleteAsync(apiBaseUrl + "UserLocation/" + id))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                ViewBag.Result = "Success";
            }

            return RedirectToAction("Index");
        }
    }
}
