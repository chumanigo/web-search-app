﻿using ImageSearch.WebApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ImageSearch.WebApp.Controllers
{
    [Authorize]
    public class LocationController : Controller
    {
        private readonly ILogger<LocationController> _logger;
        private IConfiguration _Configure;
        private string apiBaseUrl;

        HttpClient client;
        public LocationController(ILogger<LocationController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _Configure = configuration;
            apiBaseUrl = _Configure.GetValue<string>("WebAPIBaseUrl");

            client = new HttpClient
            {
                BaseAddress = new Uri(apiBaseUrl + "Location")
            };
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        }
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            List<Location> LocationList = new List<Location>();
            using (var response = await client.GetAsync(apiBaseUrl + "Location"))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                LocationList = JsonConvert.DeserializeObject<List<Location>>(apiResponse);
            }

            return View(LocationList);
        }
        public ViewResult GetLocation() => View();

        /// <summary>
        /// Gets locations based on the parameters below
        /// </summary>
        /// <param name="datasource"></param>
        /// <param name="name"></param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetLocation(string datasource, string name, double latitude, double longitude)
        {
            List<Location> location = new List<Location>();
            StringBuilder query = new StringBuilder();

            //-- Build query string
            if (!string.IsNullOrEmpty(name))
            {
                query.Append(name);
            }
            else if (!string.IsNullOrEmpty(latitude.ToString()) &&
                !string.IsNullOrEmpty(longitude.ToString()))
            {
                query.Append(latitude + "," + longitude);
            }

            //-- attempt fetching existing data from database
            if (datasource.ToLower() == "db")
            {
                using (var response = await client.GetAsync(apiBaseUrl + "Location/" + query.ToString()))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    location = JsonConvert.DeserializeObject<List<Location>>(apiResponse);
                }
            }
            else if (datasource.ToLower() == "api")//-- attempt fetching existing data from bing map API
            {
                using (var response = await client.GetAsync(apiBaseUrl + "BingMapSearch/query=" + query.ToString()))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    location = JsonConvert.DeserializeObject<List<Location>>(apiResponse);
                }
            }

            return View(location);
        }

        public ViewResult AddLocation() => View();

        [HttpPost]
        public async Task<IActionResult> AddLocation(Location location)
        {
            Location receivedLocation = new Location();

            StringContent content = new StringContent(JsonConvert.SerializeObject(location), Encoding.UTF8, "application/json");

            using (var response = await client.PostAsync(apiBaseUrl + "Location", content))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                receivedLocation = JsonConvert.DeserializeObject<Location>(apiResponse);
            }

            return View(receivedLocation);
        }

        /// <summary>
        /// Maps user to the location
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SetUserLocation(int id)
        {
            if (id == 0)
                return NotFound();

            UserLocation userlocation = new UserLocation();
            userlocation.Username = User.Identity.Name;
            userlocation.LocationId = id;

            StringContent content = new StringContent(JsonConvert.SerializeObject(userlocation), Encoding.UTF8, "application/json");

            using (var response = await client.PostAsync(apiBaseUrl + "UserLocation", content))
            {
                string apiResponse = await response.Content.ReadAsStringAsync();
                userlocation = JsonConvert.DeserializeObject<UserLocation>(apiResponse);
            }

            return RedirectToAction("GetLocation");
        }
    }
}     

