﻿using ImageSearch.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ImageSearch.WebApp.ViewModels
{
    public class BingSearchImageRow : ViewComponent
    {
        public IViewComponentResult Invoke(List<ImageInfo> images)
        {
            return View(images);
        }
    }
}
