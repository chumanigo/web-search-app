﻿using ImageSearch.WebApp.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ImageSearch.WebApp.ViewModels
{
    public class BingSearchImageGrid : ViewComponent
    {
        public IViewComponentResult Invoke(List<ImageInfo> images)
        {
            return View(images);
        }
    }
}
