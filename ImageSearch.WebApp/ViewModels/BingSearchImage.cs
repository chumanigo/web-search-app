﻿using ImageSearch.WebApp.Models;
using Microsoft.AspNetCore.Mvc;

namespace ImageSearch.WebApp.ViewModels
{
    public class BingSearchImage : ViewComponent
    {
        public IViewComponentResult Invoke(ImageInfo image)
        {
            return View(image);
        }
    }
}
