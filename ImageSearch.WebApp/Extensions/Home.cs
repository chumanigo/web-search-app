﻿using System;

namespace ImageSearch.WebApp.Extensions
{
    public static class Home
    {
        public static string GetProxyUrl(Uri uri)
        {
            string functionUrl = "https://function.bitscry.com/api/imageproxy";
            functionUrl = functionUrl + "?url=" + uri.ToString();

            return functionUrl;
        }

    }
}
