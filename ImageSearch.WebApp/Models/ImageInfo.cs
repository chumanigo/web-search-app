﻿using System;

namespace ImageSearch.WebApp.Models
{
    public class ImageInfo
    {
        public int ImageInfoId { get; set; }
        public string Name { get; set; }
        public DateTime? DatePublished { get; set; }
        public string HomePageUrl { get; set; }
        public int? ContentSize { get; set; }
        public double? Width { get; set; }
        public double? Height { get; set; }
        public string ImageId { get; set; }
        public string AccentColor { get; set; }
        public string WebSearchUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string EncodingFormat { get; set; }
        public string ContentUrl { get; set; }
        public double? ThumbnailWidth { get; set; }
        public double? ThumbnailHeight { get; set; }
    }
}
