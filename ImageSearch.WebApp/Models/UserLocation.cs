﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageSearch.WebApp.Models
{
    public class UserLocation
    {
        public int UserLocationId { get; set; }
        public string Username { get; set; }
        public int LocationId { get; set; }
        public virtual Location Location { get; set; }
    }
}
