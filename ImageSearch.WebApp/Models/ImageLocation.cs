﻿namespace ImageSearch.WebApp.Models
{
    public class ImageLocation
    {
        public int ImageLocationId { get; set; }
        public int ImageInfoId { get; set; }
        public int? LocationId { get; set; }

        public virtual Location Location { get; set; }
    }
}
