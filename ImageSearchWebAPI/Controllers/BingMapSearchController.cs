﻿using ImageSearchWebAPI.Model;
using ImageSearchWebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BingMapSearchController : ControllerBase
    {
         #region Constructor   
        private readonly IConfiguration _config;
        public BingMapSearchController(IConfiguration config)
        {
            _config = config;
        }
        #endregion

        /// <summary>
        /// Fetch location base on the query -bing Map search
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("{query}")]
        public async Task<IEnumerable<Location>> GetAsync(string query)
        {
            string cleanString = Helper.RemoveSpecialCharacters(query.Substring(6));
            var queryString = _config["Keys:BingMapEndpoint"]  + cleanString + "?key=" + _config["Keys:BingMapSearchKey"];

            // Perform the Web request and get the response
            WebRequest request = HttpWebRequest.Create(queryString);
            
            HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result;
            string json = new StreamReader(response.GetResponseStream()).ReadToEnd();

            dynamic parsedJson = JsonConvert.DeserializeObject(json);

            ResourceResuts resourceResuts = JsonConvert.DeserializeObject<ResourceResuts>(JsonConvert.SerializeObject(parsedJson, Formatting.Indented));

            //-- Create list of Location that is the database entity
            List<Location> locationList = new List<Location>();
            locationList.AddRange(CovertToLocation(resourceResuts.resourceSets));

            //-- Save data to local database if doesn't exist
            await SaveToLocalDBAsync(locationList);

            return locationList;
        }

        /// <summary>
        /// Create object matching database Location from Bing Map Search API
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        private List<Location> CovertToLocation(List<ResourceSet> values)
        {
            List<Location> locationList = new List<Location>();

            foreach (var resourceSet in values)
            {
                if (resourceSet != null)
                {
                    foreach (var resource in resourceSet.resources)
                    {
                        Location loc = new Location();
                        loc.LocationName = resource.address.locality;
                        loc.Latitude = resource.point.coordinates[0];
                        loc.Longitude = resource.point.coordinates[1];
                        loc.LocationDesc = resource.name + ", "+resource.address.countryRegion;
                        locationList.Add(loc);
                    }
                }
            }

            return locationList;
        }

        /// <summary>
        ///  Save new locations in the database
        /// </summary>
        /// <param name="locationList"></param>
        /// <returns></returns>
        public async Task SaveToLocalDBAsync(List<Location> locationList)
        {
            LocationService location = new LocationService();
            await location.AddLocationAsyn(locationList);
        }
    }
    public class Point
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }
    public class Address
    {
        public string addressLine { get; set; }
        public string adminDistrict { get; set; }
        public string adminDistrict2 { get; set; }
        public string countryRegion { get; set; }
        public string formattedAddress { get; set; }
        public string locality { get; set; }
        public string postalCode { get; set; }
    }
    public class GeocodePoint
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
        public string calculationMethod { get; set; }
        public List<string> usageTypes { get; set; }
    }
    public class Resource
    {
        public string __type { get; set; }
        public List<double> bbox { get; set; }
        public string name { get; set; }
        public Point point { get; set; }
        public Address address { get; set; }
        public string confidence { get; set; }
        public string entityType { get; set; }
        public List<GeocodePoint> geocodePoints { get; set; }
        public List<string> matchCodes { get; set; }
    }
    public class ResourceSet
    {
        public int estimatedTotal { get; set; }
        public List<Resource> resources { get; set; }
    }
    public class ResourceResuts
    {
        public string authenticationResultCode { get; set; }
        public string brandLogoUri { get; set; }
        public string copyright { get; set; }
        public List<ResourceSet> resourceSets { get; set; }
        public int statusCode { get; set; }
        public string statusDescription { get; set; }
        public string traceId { get; set; }
    }
}
