﻿using ImageSearchWebAPI.Interfaces;
using ImageSearchWebAPI.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace ImageSearchWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserLocationController : ControllerBase
    {
        #region Constructor
        private readonly IUserLocation _repository;
        public UserLocationController(IUserLocation userlocation) => _repository = userlocation;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<UserLocation> Get() => _repository.UserLocations;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("{name}")]
        public List<UserLocation> Get(string name)
        {
            return _repository.UserLocations.Where(x=>x.Username == name).ToList();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userloc"></param>
        /// <returns></returns>
        [HttpPost]
        public UserLocation Post([FromBody] UserLocation userloc) =>
        _repository.AddUserLocation(new UserLocation
        {
            Username = userloc.Username,
            LocationId = userloc.LocationId
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userloc"></param>
        /// <returns></returns>
        [HttpPut]
        public UserLocation Put([FromBody] UserLocation userloc) => _repository.UpdateUserLocation(userloc);

        [HttpDelete("{id}")]
        public void Delete(int id) => _repository.DeleteUserLocation(id);
    }
}
