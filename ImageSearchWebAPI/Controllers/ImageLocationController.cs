﻿using ImageSearchWebAPI.Interfaces;
using ImageSearchWebAPI.Model;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ImageSearchWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageLocationController : ControllerBase
    {
		#region Constructor
        private readonly IImageLocation _repository;
        public ImageLocationController(IImageLocation imagelocation)=>  _repository = imagelocation;
        #endregion
		
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		[HttpGet]
        public IEnumerable<ImageLocation> Get() => _repository.ImageLocations;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ImageLocation Get(int id) => _repository[id];


        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageloc"></param>
        /// <returns></returns>
        [HttpPost]
        public ImageLocation Post([FromBody] ImageLocation imageloc) =>
        _repository.AddImageLocation(new ImageLocation
        {
            ImageInfoId  = imageloc.ImageInfoId,
            LocationId = imageloc.LocationId
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="imageloc"></param>
        /// <returns></returns>
        [HttpPut]
        public ImageLocation Put([FromBody] ImageLocation imageloc) => _repository.UpdateImageLocation(imageloc);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        [HttpPatch("{id}")]
        public StatusCodeResult Patch(int id, [FromBody] JsonPatchDocument<ImageLocation> patch)
        {
            ImageLocation imageloc = Get(id);
            if (imageloc != null)
            {
                patch.ApplyTo(imageloc);
                return Ok();
            }
            return NotFound();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id) => _repository.DeleteImageLocation(id);
    }
}
