﻿using ImageSearchWebAPI.Interfaces;
using ImageSearchWebAPI.Model;
using ImageSearchWebAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BingImageSearchController : Controller
    {
        //-- This controller uses Azure Bing Search V7 (subscription key and endpoint added to your environment variables).

        #region Constructor
        private readonly IImageService _repository;
        private readonly IConfiguration _config;
        public BingImageSearchController(IImageService imageService, IConfiguration config)
        {
            _repository = imageService;
            _config = config;
        }
        #endregion

        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Fetch images base on the query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        [HttpGet("{query}")]
        public async Task<IEnumerable<ImageInfo>> GetListAsync(string query)
        {
            Console.OutputEncoding = Encoding.UTF8;
            string cleanString = Helper.RemoveSpecialCharacters(query.Substring(6));
            // Construct the URI of the search request
            var uriQuery = _config["Keys:Endpoint"] + "?q=" + Uri.EscapeDataString(cleanString);

            // Perform the Web request and get the response
            WebRequest request = HttpWebRequest.Create(uriQuery);
            request.Headers["Ocp-Apim-Subscription-Key"] = _config["Keys:ComputerVision"]; 
            request.Headers["X-Search-Location"] = "lat:-29.870433807373047;long:30.98834228515625;re:100";

            HttpWebResponse response = (HttpWebResponse)request.GetResponseAsync().Result;
            string json = new StreamReader(response.GetResponseStream()).ReadToEnd();

            dynamic parsedJson = JsonConvert.DeserializeObject(json);
             
            SearchResult info = JsonConvert.DeserializeObject<SearchResult>(JsonConvert.SerializeObject(parsedJson, Formatting.Indented));

            //-- Create list of ImageInfo that is the database entity
            List<ImageInfo> imageInfoList = new List<ImageInfo>();
            imageInfoList.AddRange(CovertToImageInfo(info.value));

            //-- Save data to local database if doesn't exist
            await SaveToLocalDBAsync(imageInfoList);

            return imageInfoList;            
        }

        /// <summary>
        /// Save new images in the database
        /// </summary>
        /// <param name="imageInfoList"></param>
        /// <returns></returns>
        public async Task SaveToLocalDBAsync(List<ImageInfo> imageInfoList)
        {
            ImageService imageService = new ImageService();
            await imageService.AddImageInfoAsyn(imageInfoList);
        }

        /// <summary>
        ///  Create object matching database ImageInfo from Bing Image Search API
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        private List<ImageInfo> CovertToImageInfo(List<Value> values)
        {
            List<ImageInfo> imageInfoList = new List<ImageInfo>();

            foreach (var value in values)
            {
                ImageInfo info = new ImageInfo();
                info.WebSearchUrl = value.webSearchUrl;
                info.Name = value.name;
                info.ThumbnailUrl = value.thumbnailUrl;
                info.DatePublished = value.datePublished;
                info.ContentUrl = value.contentUrl;
                info.HomePageUrl = value.hostPageUrl;
                int.TryParse(value.contentSize.Split(' ')[0], out int contentSize);
                info.ContentSize = contentSize;
                info.EncodingFormat = value.encodingFormat;
                //info.hostPageDisplayUrl = value.hostPageDisplayUrl;
                info.Width = value.width;
                info.Height = value.height;
                //info.imageInsightsToken = value.imageInsightsToken;
                info.ImageId = value.imageId;
                info.AccentColor = value.accentColor;

                imageInfoList.Add(info);
            }
            return imageInfoList;
        }
    }
    public class Instrumentation
    {
        public string _type { get; set; }
    }
    public class Thumbnail
    {
        public int width { get; set; }
        public int height { get; set; }
    }
    public class BestRepresentativeQuery
    {
        public string text { get; set; }
        public string displayText { get; set; }
        public string webSearchUrl { get; set; }
    }
    public class InsightsMetadata
    {
        public int recipeSourcesCount { get; set; }
        public BestRepresentativeQuery bestRepresentativeQuery { get; set; }
        public int pagesIncludingCount { get; set; }
        public int availableSizesCount { get; set; }
    }
    public class Value
    {
        public string webSearchUrl { get; set; }
        public string name { get; set; }
        public string thumbnailUrl { get; set; }
        public DateTime datePublished { get; set; }
        public string contentUrl { get; set; }
        public string hostPageUrl { get; set; }
        public string contentSize { get; set; }
        public string encodingFormat { get; set; }
        public string hostPageDisplayUrl { get; set; }
        public int width { get; set; }
        public int height { get; set; }
        //public Thumbnail thumbnail { get; set; }
        public string imageInsightsToken { get; set; }
        //public InsightsMetadata insightsMetadata { get; set; }
        public string imageId { get; set; }
        public string accentColor { get; set; }
    }
    public class SearchResult
    {
        public string _type { get; set; }
        public Instrumentation instrumentation { get; set; }
        public string readLink { get; set; }
        public string webSearchUrl { get; set; }
        public int totalEstimatedMatches { get; set; }
        public int nextOffset { get; set; }
        public List<Value> value { get; set; }
    }
 
}
