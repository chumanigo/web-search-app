﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImageSearchWebAPI.Model;
using ImageSearchWebAPI.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

namespace ImageSearchWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LocationController : ControllerBase
    {
        #region Constructor
        private readonly ILocation _repository;
        public LocationController(ILocation location) => _repository = location;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<Location> Get() => _repository.Locations;

        [HttpGet("{name}")]
        public IEnumerable<Location> Get(string name)
        {
            return _repository.GetByName(name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("id/{id}")]
        public Location GetByID(int id) {
            var value = _repository.Locations.Where(x=>x.LocationId == id).FirstOrDefault();
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <returns></returns>
        [HttpGet("{latitude},{longitude}")]
        public Location GetByID(double latitude, double longitude)
        {
            var value = _repository.Locations.Where(x => x.Latitude == latitude && x.Longitude == longitude).FirstOrDefault();
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loc"></param>
        /// <returns></returns>
        [HttpPost]
        public Location Post([FromBody] Location loc) =>
        _repository.AddLocation(new Location
        {
            LocationName = loc.LocationName,
            LocationDesc = loc.LocationDesc,
            Latitude = loc.Latitude,
            Longitude = loc.Longitude
        });

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loc"></param>
        /// <returns></returns>
        [HttpPut]
        public Location Put([FromBody] Location loc) => _repository.UpdateLocation(loc);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="patch"></param>
        /// <returns></returns>
        [HttpPatch("id/{id}")]
        public StatusCodeResult Patch(int id, [FromBody] JsonPatchDocument<Location> patch)
        {
            Location loc = GetByID(id);
            if (loc != null)
            {
                patch.ApplyTo(loc);
                return Ok();
            }
            return NotFound();
        }
       
    }
}
