﻿using ImageSearchWebAPI.Interfaces;
using ImageSearchWebAPI.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace ImageSearchWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {

        #region Constructor
        private readonly IImageService _repository;

        public ImageController(IImageService imageService) => _repository = imageService;
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<ImageInfo> Get() => _repository.Images;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet("{name}")]
        public IEnumerable<ImageInfo> Get(string name)
        {
            return _repository.GetByName(name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id) => _repository.DeleteImageInfo(id);		

    }
}
