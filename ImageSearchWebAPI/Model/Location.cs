﻿using System;
using System.Collections.Generic;

namespace ImageSearchWebAPI.Model
{
    public partial class Location
    {
        public Location()
        {
            ImageLocation = new HashSet<ImageLocation>();
            UserLocation = new HashSet<UserLocation>();
        }

        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string LocationDesc { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public virtual ICollection<ImageLocation> ImageLocation { get; set; }
        public virtual ICollection<UserLocation> UserLocation { get; set; }
    }
}
