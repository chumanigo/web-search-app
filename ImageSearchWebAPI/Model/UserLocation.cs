﻿using System;
using System.Collections.Generic;

namespace ImageSearchWebAPI.Model
{
    public partial class UserLocation
    {
        public int UserLocationId { get; set; }
        public string Username { get; set; }
        public int LocationId { get; set; }

        public virtual Location Location { get; set; }
    }
}
