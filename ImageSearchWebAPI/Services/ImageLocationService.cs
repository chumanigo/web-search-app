﻿using ImageSearchWebAPI.Model;
using ImageSearchWebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ImageSearchWebAPI.Services
{
    public class ImageLocationService : IImageLocation
    {
        #region Properties
        private ImageSearchWebContext _repository { get; set; }		
        #endregion

        public ImageLocationService()
        {
            _repository = new ImageSearchWebContext();
        }

        public ImageLocation Get(double latitude, double longitude)
        {
            throw new NotImplementedException();
        }
		
        public ImageLocation this[int id] => _repository.ImageLocation.Find(id);

        public IEnumerable<ImageLocation> ImageLocations => _repository.ImageLocation.ToList();
		
        public ImageLocation AddImageLocation(ImageLocation imageLocation)
        {
            //TODO

            return imageLocation;
        }

        public void DeleteImageLocation(int id) => _repository.ImageLocation.Remove(_repository.ImageLocation.Find(id));

        public ImageLocation UpdateImageLocation(ImageLocation imageLocation) => AddImageLocation(imageLocation);
    }
}