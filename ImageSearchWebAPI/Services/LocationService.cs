﻿using ImageSearchWebAPI.Model;
using ImageSearchWebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Services
{
    public class LocationService : ILocation
    {
        #region Properties
        private ImageSearchWebContext _repository { get; set; }

        #endregion

        public LocationService()
        {
            _repository = new ImageSearchWebContext();
        }
        public Location this[int id] => _repository.Location.Find(id);
        public IEnumerable<Location> Locations => _repository.Location.ToList();
        public IEnumerable<Location> this[string name] => _repository.Location.Where(x => x.LocationName.Contains(name)).ToList();
        public Location AddLocation(Location location)
        {
            if (location.LocationId == 0)
            {
                _repository.Attach(location);
                _repository.Entry(location).State = EntityState.Added;
                _repository.SaveChanges();
            }

            return location;
        }
        public Location UpdateLocation(Location userLocation) => AddLocation(userLocation);
        public async Task AddLocationAsyn(List<Location> location)
        {
            //--  Save new location in the database, ignore locations that are already in the db.
            //-- unique index geo location point(lat , long) or discription which includes address, city and country.

            foreach (var item in location)
            {
                var fetchFromDB = _repository.Location.Where(x => (x.Latitude == item.Latitude &&
                x.Longitude == item.Longitude) || x.LocationDesc.Trim() == item.LocationDesc.Trim()
                ).FirstOrDefault();

                if (fetchFromDB == null)
                {
                    await _repository.AddAsync(item);
                }
            }

            await _repository.SaveChangesAsync();
        }
        public IEnumerable<Location> GetByName(string name)
        {
            return _repository.Location.Where(x => x.LocationName.Contains(name) || 
            x.LocationDesc.Contains(name)).ToList();
        }
    }
}