﻿using ImageSearchWebAPI.Model;
using ImageSearchWebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace ImageSearchWebAPI.Services
{
    public class ImageService: IImageService
    {
        #region Properties
        private ImageSearchWebContext _repository { get; set; }
        #endregion

        public ImageService()
        {
            _repository = new ImageSearchWebContext();
        }

        public ImageInfo Get(double latitude, double longitude)
        {
            throw new NotImplementedException();
        }
        public ImageInfo this[int id] => _repository.ImageInfo.Find(id);
        public IEnumerable<ImageInfo> Images => _repository.ImageInfo.ToList();

        public async Task AddImageInfoAsyn(List<ImageInfo> imageInfo)
        {
            //--  Save new images in the database, ignore images that are already in the db.
            //-- unique index in imageid

            foreach (var item in imageInfo)
            {
                var fetchFromDB = _repository.ImageInfo.Where(x => x.ImageId == item.ImageId).FirstOrDefault();
                if (fetchFromDB == null)
                {
                    await _repository.AddAsync(item);
                }
            }

            await _repository.SaveChangesAsync();
        }
        public void DeleteImageInfo(int id) => _repository.Remove(_repository.ImageInfo.Find(id));

        public IEnumerable<ImageInfo> GetByName(string name)
        {
            return _repository.ImageInfo.Where(x => x.Name.Contains(name)).ToList();
        }
    }
}
