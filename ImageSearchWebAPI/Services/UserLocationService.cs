﻿using ImageSearchWebAPI.Model;
using ImageSearchWebAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ImageSearchWebAPI.Services
{
    public class UserLocationService : IUserLocation
    {
        #region Properties
        private ImageSearchWebContext _repository { get; set; }
        #endregion

        public UserLocationService()
        {
            _repository = new ImageSearchWebContext();
        }

        public UserLocation Get(double latitude, double longitude)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<UserLocation> UserLocations => _repository.UserLocation.ToList();

        public UserLocation this[string name] => _repository.UserLocation.Where(x => x.Username == name).FirstOrDefault();

        public UserLocation AddUserLocation(UserLocation userLocation)
        {
            if (userLocation.UserLocationId == 0)
            {
                _repository.Add(userLocation);
                _repository.SaveChanges();
            }
            return userLocation;
        }

        public void DeleteUserLocation(int id)
        {
            if (id == 0)
                return;

            _repository.UserLocation.Remove(_repository.UserLocation.Find(id));
            _repository.SaveChanges();
        }

        public UserLocation UpdateUserLocation(UserLocation userLocation) => AddUserLocation(userLocation);

        public UserLocation Get(int UserlocationID) => _repository.UserLocation.Find(UserlocationID);

        public IEnumerable<UserLocation> UserLocationsByUser(string username)
        {
           return _repository.UserLocation.Where(x => x.Username == username).ToList();
        }
    }
}