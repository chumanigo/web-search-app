﻿using ImageSearchWebAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Interfaces
{
    public interface ILocation
    {
        IEnumerable<Location> GetByName(string name);
        IEnumerable<Location> Locations { get; }
        Task AddLocationAsyn(List<Location> location);
        IEnumerable<Location> this[string name] { get; }
        Location AddLocation(Location location);
        Location UpdateLocation(Location location); 
    }
}
