﻿using ImageSearchWebAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Interfaces
{
    public interface IUserLocation
    {           
        IEnumerable<UserLocation> UserLocations { get; }
        UserLocation this[string name] { get; }
        IEnumerable<UserLocation> UserLocationsByUser(string username);
        UserLocation AddUserLocation(UserLocation location);
        UserLocation UpdateUserLocation(UserLocation location);
        void DeleteUserLocation(int id);
        UserLocation Get(int UserlocationID);        
    }
}
