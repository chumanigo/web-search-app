﻿using ImageSearchWebAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Interfaces
{
    public interface IImageLocation
    {           
        IEnumerable<ImageLocation> ImageLocations { get; }
        ImageLocation this[int id] { get; }
        ImageLocation AddImageLocation(ImageLocation imagelocation);
        ImageLocation UpdateImageLocation(ImageLocation imagelocation);
        void DeleteImageLocation(int id);                
    }
}
