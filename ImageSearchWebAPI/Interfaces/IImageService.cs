﻿using ImageSearchWebAPI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Interfaces
{
    public interface IImageService
    {
        IEnumerable<ImageInfo> Images { get; }
        public IEnumerable<ImageInfo> GetByName(string name);
        ImageInfo this[int id] { get; }
        Task AddImageInfoAsyn(List<ImageInfo> imageInfo);
        void DeleteImageInfo(int id);  
    }
}
