﻿using ImageSearchWebAPI.Interfaces;
using ImageSearchWebAPI.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ImageSearchWebAPI.Dependencies
{
    public static class ServicesDependency
    {
        public static void AddServiceDependency(this IServiceCollection services)
        {
            services.AddScoped<IImageService, ImageService>();
			services.AddScoped<ILocation, LocationService>();
			services.AddScoped<IUserLocation, UserLocationService>();
            services.AddScoped<IImageLocation, ImageLocationService>();
        }
    }
}
