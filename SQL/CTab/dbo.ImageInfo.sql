SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ImageInfo](
	[ImageInfoID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[DatePublished] [date] NULL,
	[HomePageUrl] [nvarchar](256) NULL,
	[ContentSize] int NULL,
	[Width] [float] NULL,
	[Height] [float] NULL,
	[ImageId] [nvarchar](256) NULL,
	[AccentColor] [nvarchar](256) NULL,
	[WebSearchUrl] [nvarchar](256) NULL,
	[ThumbnailUrl] [nvarchar](256) NULL,
	[EncodingFormat] [nvarchar](10) NULL,
	[ContentUrl] [nvarchar](256) NULL,
	[ThumbnailWidth] [float] NULL,
	[ThumbnailHeight] [float] NULL,
 CONSTRAINT [PK_ImageInfo] PRIMARY KEY CLUSTERED 
(
	[ImageInfoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


