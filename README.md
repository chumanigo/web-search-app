# Web Search App Project

Web Search application - developed and tested using google chrome

- Execute following scripts
  1. Create Database -  ImageSearchWeb
  2. Update appsettings.json connection strings for both projects to the correct Server and database
     -(Also update ImageSearchWebContext in OnConfiguring() method)
  3. Create script(Creating tables) SQL\CTab\
  4. Indexes  SQL\Indexes  
  
- If running solution from VS, setup solution for multiple startup
  1. Right click on solution -> properties -> Select Multiple startup projects -> set both project actions to start
  2. Should b set to run the project 
	
	